'''
    in this example, we introduce some debugging facility
    including _debug_ and _online_ variables. This allows the listener to take a locally stored tweet instead of a live connection. We also save a twitter object in a local file for the purpose of debugging the listener.
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation
from pylib import utils

#if the tweet entities contain utf characters, we must do some special treatment.
utils.enable_utf_print()

_debug_ = True #implement debugging behavior
_online_ = False #read from live stream

class CustomStreamListener(tweepy.StreamListener):
    def on_data(self, raw_tweet):
        #on_data provides data in the raw json string
        try:        
            tweet = json.loads(raw_tweet)
            if tweet.get('delete'):
                return True ## skip delete tweet
            # if tweet.get('lang') != 'en':
                # return False            
        
            print "%s, %s\n%s\n\n"%(tweet['created_at'],tweet['user']['name'],tweet['text'])
        
        except Exception, e:
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_json(tweet,'debug/tweet.txt')
                raise #stop the listener
            else:
                pass #ignore parsing errors
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listen

if __name__ == '__main__':
    #create a listener object
    listener = CustomStreamListener()        
    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream,
        stream = tweepy.streaming.Stream(auth, listener)
        
        #you may choose to track certain topics
        #stream.filter(track=['#bigdata','big data'])    
        
        #or receive a sample of all tweets - that's a lot!
        stream.sample()

        while True:
            try:
                if stream.running is False:
                    print 'Stream stopped!'
                    break
                time.sleep(1) #sleep for 1 sec
            except KeyboardInterrupt:
                break
                
        stream.disconnect()
        print 'Bye!'
    else:
        #offline mode
        print "offline mode"
        #listener.on_data(utils.read_file('debug/tweet_jp.txt'))
        listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))
        exit(0)