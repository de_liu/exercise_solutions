'''
    in this example, we do some deeper parsing,
    extract useful information in a tweet into dict objects
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation
from pylib import utils

_debug_ = True #implement debugging behavior
_online_ = False #read from live stream

utils.enable_utf_print() 

#these fields can be directly copied to tweets table
tweet_fields = {
    'id':'tweet_id',
    'text':'tweet_text',
    'source':'source',
    'retweet_count':'retweet_count',    
    'in_reply_to_status_id':'reply_tweet_id',
    'in_reply_to_user_id':'reply_user_id',
    'favorite_count':'favorite_count',
}

#to users table
user_fields = {
    'name':'name',
    'screen_name':'screen_name',
    'id':'user_id',
}

def get_fields(source, fields):
    '''
        given a source and a destination dict, copy the fields from source to destination according to the mapping provided by fields.  e.g. fields['fielda']='fieldb' would map fielda in source to fieldb in dest.     
    '''
    dest={}    
    if not isinstance(source, dict):
        print "source is not a dict" + type(source)

    for field in fields:
        if source.get(field):
            dest[fields[field]]=source[field]
    return dest 

class CustomStreamListener(tweepy.StreamListener):

    def on_data(self, raw_tweet):
        #on_data provides data in the raw json string
        try:    
            #record the time of event
            capture_date = utils.mysql_time()
            
            tweet = json.loads(raw_tweet)
            if tweet.get('delete'):
                return True ## skip delete tweet
            # if tweet.get('lang') != 'en':
                # return False    
                
            tweet_data = {}
            user_data = {}
            
            # retrieve and store tweet_data    
            tweet_data.update(get_fields(tweet,tweet_fields))
            
            if not tweet_data.get('tweet_id'):
                if _debug_:
                    #save the tweet for debugging
                    utils.dump_json(tweet,'debug/tweet.txt')
                print('No tweet id')
                return True  #discard this tweet
            
            #fields that need special processing
            if tweet_data.get('created_at'):
                tweet_data['created_at']=utils.utc_to_mysql_time(tweet_data['created_at'])
            
            if tweet.get('user'):
                # retrieve most of fields.
                user_data.update(get_fields(tweet['user'],user_fields))    
                if not user_data.get('user_id'):
                    if _debug_:
                        utils.dump_json(tweet,'debug/tweet%s.txt'%tweet_data.get('tweet_id'))
                    print('No tweet user id')
                    return True  #discard this tweet 
                    
            #enrichment    
            if tweet_data: 
                tweet_data['user_id'] = user_data.get('user_id')
                tweet_data['capture_date']=capture_date
                print "tweet_data"
                print tweet_data
                print
            if user_data:
                user_data['capture_date']=capture_date
                print "user_data"
                print user_data
                print
        except Exception, e:
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_json(tweet,'debug/tweet.txt')
                raise 
            else:
                pass #ignore errors
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listening

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()
    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream
        stream = tweepy.streaming.Stream(auth, listener)
        
        #you may choose to track certain topics
        #stream.filter(track=['#bigdata','big data'])    
        
        #or receive a sample of all tweets - that's a lot!
        stream.sample()

        while True:
            try:
                if stream.running is False:
                    print 'Stream stopped!'
                    break
                time.sleep(1) #sleep for 1 sec
            except KeyboardInterrupt:
                break
                
        stream.disconnect()
        print 'Bye!'
    else:
        #offline behavior
        #listener.on_data(utils.read_file('debug/tweet_jp.txt'))
        listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))
        exit(0)