'''
	test configuration.
	
'''

import config #put config.py in the same directory
import json
import urllib2 #needs installation
import facebook
import facepy
from pylib import utils

if __name__ == '__main__':
    print "if you do not see error messages, then you're ready to go"
    print "Because facebook ACCESS_TOKEN is short-lived, we are not testing it here"
    print "if facepy is missing, install it by typing: easy_install facepy"
    print "if facebook-sdk is missing, install it by typing: easy_install facebook-sdk"
    print "if config is missing, save config.txt as config.py. no need to configure it at this time"
    print 

    
    