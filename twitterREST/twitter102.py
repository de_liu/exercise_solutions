# -*- coding: utf-8 -*-
#------------------------------------- 
# # Twitter 102 - Search and Analyzing Tweets (solution)
#------------------------------------- 


import twitter
import json
import config

# load config data from config
auth = twitter.oauth.OAuth(config.oauth_token, config.oauth_token_secret,
config.consumer_key, config.consumer_secret)

#make a twitter api object with the OAuth object
api = twitter.Twitter(auth=auth)

print api #this line for testing that we have a good connection.

#------------------------------------- 
# ##1. conducting search using search.tweets
#------------------------------------- 

#enter the search words here, q can include keywords, hashtags, and mentions @username.
q = '#bigdata' 
count = 100 #the maximum is 100, according twitter API documentation

search_results = api.search.tweets(q=q, count=count)
statuses = search_results['statuses']

#print one sample tweet 
print json.dumps(statuses[0], indent=2)

#------------------------------------- 
# ##2. Extract Mentions, Hashtags, and Words
#------------------------------------- 

status_texts = [ status['text'] 
                for status in statuses ]

# user mentions can be found under, statuses->entities->user_mentions
screen_names = [ user_mention['screen_name'] 
                for status in statuses
                    for user_mention in status['entities']['user_mentions']] 
                    
# hashtags can be found under, statuses->entities->hastags
hashtags = [ hashtag['text']
            for status in statuses 
                for hashtag in status['entities']['hashtags'] ]

# Compute a collection of all words from all tweets
words = [ w for t in status_texts for w in t.split() ]
#

#------------------------------------- 
# Print a sample of statuses, mentions, hashtags, and words. We use list 
# slicing 0:5 to get the first five.
#------------------------------------- 

print json.dumps(status_texts[0:5], indent=1)
print json.dumps(screen_names[0:5], indent=1)
print json.dumps(hashtags[0:5], indent=1)
print json.dumps(words[0:5], indent=1)

#------------------------------------- 
# ##3. Frequence Distribution and Top Words
#------------------------------------- 


#Counter object from package collections is useful for finding out the most frequent ones
from collections import Counter 

for item in [words, screen_names, hashtags]:
    # create a counter project
    c = Counter(item)
    print c.most_common()[:10] # top 10
    print

#------------------------------------- 
# We can pretty results in a table using the prettytable package.
#------------------------------------- 

from prettytable import PrettyTable


# we are printing three tables at once.
for label, data in (('Word', words), ('Screen Name', screen_names), ('Hashtag', hashtags)):
    #field names are provided in a list.
    pt = PrettyTable([label, 'Count']) 
    c = Counter(data) 
    # add rows one by one based on the top 10 from Counter
    [ pt.add_row(kv) for kv in c.most_common()[:10] ] 
    #counter object's most_common() method returns a row.
    pt.align[label], pt.align['Count'] = 'l', 'r' # Set column alignment
    print pt


#------------------------------------- 
# ##4. Find the most popular tweets using retweet_count
#------------------------------------- 

retweets = [
    (status['retweet_count'], status['retweeted_status']['user']['screen_name'], status['text']) #track these three values
    for status in statuses # for each status
        if status.has_key("retweeted_status") # as long as the status has been retweeted
]

pt = PrettyTable(field_names=['count','screen name','text'])
#add the top 5 rows of a sorted table (by first column)
[pt.add_row(row) for row in sorted(retweets, reverse=True)[:5]] 
pt.max_width['text']=50 #to allow for a wider column.
pt.align='l' #all columns are left aligned.
print pt

#------------------------------------- 
# ##5. Plotting frequencies of words - loglog plot
#------------------------------------- 


from pylab import plt
# descending sort of word frequencies.
word_counts = sorted(Counter(words).values(), reverse=True)
plt.loglog(word_counts) #log both x and y axies. 
plt.ylabel("freq")
plt.xlabel("word rank")


#------------------------------------- 
#produce a histogram for the frequence of word use 
#------------------------------------- 
c = Counter(words)
plt.hist(c.values())
plt.title("Words")
plt.ylabel("Num of items in the bin")
plt.xlabel("Bins")
plt.figure()

#------------------------------------- 
# ##6. produce a histogram using retweet_count
#------------------------------------- 

counts = [count for count, _, _ in retweets]
plt.hist(counts) # histogram
plt.ylabel("num of tweets in the bin")
plt.xlabel("bins - Num of items retweeted")
plt.figure()
