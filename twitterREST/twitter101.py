# -*- coding: utf-8 -*-
# #Twitter 101 - Get Trends (solution)


#-------------------------------------
# ##0. Configure Canopy Python - Keep Directory Synced to Editor
#-------------------------------------

# right click the Python window below, make sure "Keep Directory Synced to Editor" is checked

#-------------------------------------
# ##1. Configure Twitter API. 
#------------------------------------- 


import twitter
import config
import json

# load config data from config
auth = twitter.oauth.OAuth(config.oauth_token, config.oauth_token_secret,
config.consumer_key, config.consumer_secret)

#make a twitter api object with the OAuth object
api = twitter.Twitter(auth=auth)

print api

#------------------------------------- 
# ##2. Get Trends for specific locations
#------------------------------------- 

# see https://dev.twitter.com/docs/api/1.1/get/trends/place for details about this API endpoint
# The IDs provided below are [Yahoo! Where On Earth ID](http://developer.yahoo.com/geo/geoplanet/). To look up the WOEID of a specific place, you may use this free service http://woeid.rosselliot.co.nz/lookup/


WORLD_WOE_ID = 1
US_WOE_ID = 23424977

# Prefix ID with the underscore for query string parameterization.
# Without the underscore, the twitter package appends the ID value
# to the URL itself as a special case keyword argument.

# You'll not see results immediately, but the data is saved to the following variables. 
world_trends = api.trends.place(_id = WORLD_WOE_ID)
us_trends = api.trends.place(_id = US_WOE_ID)

#------------------------------------- 
# ##3. Print the results.
#------------------------------------- 


print world_trends
print us_trends


#------------------------------------- 
# ##4. Pretty print the results using the dumps function from Json package
# note: json package is required
#------------------------------------- 

print json.dumps(world_trends,indent=4)
print json.dumps(us_trends,indent=4)



