# --------------------------------
# #Tweepy101 - Using Tweepy to Get User Timeline and Followers (solution)
# --------------------------------

import tweepy  #different from twitter
import json
import config   #twitter OAuth configuration
import datetime
# pylib are some custom libraries provided for this course
# utils has some custom utility functions
from pylib import utils

# the authentication process for tweepy
auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
auth.set_access_token(config.oauth_token, config.oauth_token_secret)

api = tweepy.API(auth)

print api

# --------------------------------
# ##1. Get Timeline of a User
# --------------------------------

tweets = api.user_timeline(id="CarlsonNews") #id can be either screen_name or id

print len(tweets)

# we can do status.created_at instead of status['created_at'] 
# because tweepy parsed the json data and turn it into a property 
# of status object.
for status in tweets:
    print "[%s] %s" % (str(status.created_at), status.text)

# --------------------------------
# ##2. Handling Pagination Manuually
# --------------------------------

#get status after a certain day.
page = 1
eof = False #this is a user-created variable
statuses = []  #always store fetched results so that we don't have to do it again
while not eof:
    print "fetching page %i..."%page
    #twitter's user/timeline accepts page as a paramter.
    new_statuses = api.user_timeline(id='CarlsonNews', page=page)
    if new_statuses:
        statuses += new_statuses #merge two lists
        for status in new_statuses:
            #proces data here 
            
            if status.created_at < datetime.datetime(2014,6,1,0,0,0):
                print 'the rest of tweets are old'
                eof = True #no more tweets
                break
            else:
                print "[%s] %s" % (str(status.created_at), status.text)
    else:
        eof = True #there are no more tweets  
    page += 1 #go to the next page
    
    utils.my_sleep(5,10) #randomly sleep between 5 to 10 seconds

print len(statuses)


# ##3. Using Cursor to automatically handling pagination
# see tweepy documentation - cursor tutorial http://docs.tweepy.org/en/latest/cursor_tutorial.html

statuses2 = []
for s in tweepy.Cursor(api.user_timeline, id='CarlsonNews').items():
    if s.created_at < datetime.datetime(2014,8,1,0,0,0):
        print 'the rest of tweets are old'
        break
    else:
        statuses2.append(s) #store tweeet
        print "[%s] %s" % (str(s.created_at), s.text)


# ## 4. Tweepy - Get User Followers

user = api.get_user('CarlsonNews')

print "%s is followed by %i"%(user.screen_name, user.followers_count)

nfollowers = 30
print "first %i followers"%nfollowers

# return followers ordered in which they were added
followers = user.followers(count=nfollowers)

print "Screen Name, User Name, Created at"
print "\r\n".join(["%s, %s, %s"%(f.screen_name,f.name, str(f.created_at)) for f in followers])


# ## 5. Using the Cursor method

followers2 = []
for f in tweepy.Cursor(api.followers, id='CarlsonNews').items(100):
    followers2.append(f) #store follower
    print "%s, %s, %s"%(f.screen_name,f.name, str(f.created_at))

print len(followers2)

# ##6. Get follower IDs only
# twitter returns 5000 followers ID at once. This way, fewer API calls are required if all you need is follower IDs.

follower_ids = user.followers_ids(id='CarlsonNews')

print len(follower_ids)

print follower_ids[:200]


# ##7. Rate Limit Controls
# you may use tweepy's api_limit_status to get a summary of rate limits. it returns a json object.

limit = api.rate_limit_status()
#utils.print_json(limit)
print limit['resources']['followers']['/followers/ids']


