'''
	test configuration.
	
'''

import config #put config.py in the same directory
from bs4 import BeautifulSoup
from pylib import utils
import requests
import json
from pylib.simplemysql import SimpleMysql

if __name__ == '__main__':
    print "You're expected to see the first field (zpid) in the houses table"
    print "if you do not see error messages, then you're ready to go"
    print 
    #test db configuration.
    db = SimpleMysql(
        host=config.db_host,
        db=config.db_db,
        user=config.db_user,
        passwd=config.db_passwd,
        keep_alive=True
    )
    
    print db
    
    #print the first field in the house table
    print db.query("describe houses").fetchall()[0]